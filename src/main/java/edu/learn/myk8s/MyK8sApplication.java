package edu.learn.myk8s;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyK8sApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyK8sApplication.class, args);
    }

}
