package edu.learn.myk8s.controller;

import edu.learn.myk8s.repo.Book;
import edu.learn.myk8s.repo.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/books")
public class HelloController {
    private final BookRepository bookRepository;

    @PostMapping
    public Book add() {
        Book book = new Book();
        book.setName("bookName " + LocalDateTime.now());
        return bookRepository.save(book);
    }

    @GetMapping("/list")
    public List<Book> list() {
        return bookRepository.findAll();
    }

    @GetMapping("/{id}")
    public Book getById(@PathVariable(name = "id") Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Kitab yoxdur"));
    }

    @DeleteMapping("/{id}")
    public void  deleteById(@PathVariable(name = "id") Long id) {
        bookRepository.findById(id).orElseThrow(() -> new RuntimeException("Kitab yoxdur"));
        bookRepository.deleteById(id);
    }

}
