package edu.learn.myk8s.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HelloDto {
private String message;
}
